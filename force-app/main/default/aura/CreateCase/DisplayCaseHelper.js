({
    // The makeCase() method creates an asynchronous request to create a case with the submitted data. 
    // When the request completes, the callback stores the new case’s unique token in the caseID field in a variable used by the component.
    makeCase : function(component, event, helper) {
        var subject = component.find("subject").get("v.value");
        var description = component.find("description").get("v.value");
        var email = component.find("email").get("v.value");

        var action = component.get("c.CreateCase");
        action.setParams({
            "subject": subject,
            "description": description,
            "email": email
        });
        action.setCallback(this, function(response){
            component.set("v.caseID", response.getReturnValue());
        });
        $A.enqueueAction(action);
    },
    // The getCase() method uses a token entered by the guest user to asynchronously retrieve a case that matches the token. 
    // The method’s callback catches the response from the Apex method and stores the value in the case_status variable.

    getCase : function(component,event,helper){
        var case_token = component.find("existing_case").get("v.value");
        var action = component.get("c.GetCase");
        action.setParams({
            "token":case_token
        });
        action.setCallback(this, function(response){
            component.set("v.case_status", response.getReturnValue());
        });
        $A.enqueueAction(action);
    }
})
